<?php
    $json_string = file_get_contents('swaggerApiSpe.json');

    $data = json_decode($json_string, true);

    $role = [
        'type' => 'string',
        'description' => '角色位置',
        'nullable' => true,
        'oneOf' => [
            [
                '$ref' => '#/definitions/LOL_Role'
            ],
            [
                '$ref' => '#/definitions/DOTA2_Role'
            ],
            [
                '$ref' => '#/definitions/KOG_Role'
            ],
            [
                '$ref' => '#/definitions/OW_Role'
            ],
            [
                '$ref' => '#/definitions/SC2_Role'
            ],
        ],
    ];
    $schema = [
        'type' => 'object',
        'properties' => [
            'error' => [
                'type' => 'string'
            ]
        ],
    ];
    foreach ($data['paths'] as $key=>&$val){
        $val['get']['responses'][400]['description'] = 'Bad request';
        $val['get']['responses'][400]['schema'] = $schema;
        $val['get']['responses'][401]['description'] = 'Unauthorized';
        $val['get']['responses'][401]['schema'] = $schema;
        $val['get']['responses'][403]['description'] = 'Forbidden';
        $val['get']['responses'][403]['schema'] = $schema;
        $val['get']['responses'][404]['description'] = 'Not Found';
        $val['get']['responses'][404]['schema'] = $schema;
        $val['get']['responses'][422]['description'] = 'Unprocessable Entity';
        $val['get']['responses'][422]['schema'] = $schema;
        $val['get']['responses'][500]['description'] = 'Internal Server Error';
        $val['get']['responses'][500]['schema'] = $schema;

        $val['get']['produces'] = [
            "application/json"
        ];

        if($key == '/battles/abilities'){
            $val['get']['responses'][200]['schema']=[
                'type' => 'object',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/lol_abilitytimeline'
                    ],
                    [
                        '$ref' => '#/definitions/dota2_abilitytimeline'
                    ],
                ],
            ];
        }

        if($key == '/battles/items'){
            $val['get']['responses'][200]['schema']=[
                'type' => 'object',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/lol_itemtimeline'
                    ],
                    [
                        '$ref' => '#/definitions/dota2_itemtimeline'
                    ],
                ],
            ];
        }

        if($key == '/incidents/match'){
            $val['get']['responses'][200]['schema']=[
                'type' => 'object',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/matchincidents'
                    ],
                    [
                        '$ref' => '#/definitions/battleincidents'
                    ],
                ],
            ];
        }

        if($key == '/pbpdata/{match_id}/events'){
            $val['get']['responses'][200]['schema']=[
                'type' => 'object',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/csgoevents_live'
                    ],
                    [
                        '$ref' => '#/definitions/lolevents_live'
                    ],
                    [
                        '$ref' => '#/definitions/dota2events_live'
                    ]
                ],
            ];
        }
        if($key == '/pbpdata/{match_id}/frames'){
            $val['get']['responses'][200]['schema']=[
                'type' => 'object',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/csgoframes_live'
                    ],
                    [
                        '$ref' => '#/definitions/lolframes_live'
                    ],
                    [
                        '$ref' => '#/definitions/dota2frames_live'
                    ]
                ],
            ];
        }
        if($key=='/battles/events'){
            $val['get']['responses'][200]['schema']=[
                'type' => 'object',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/csgoevents'
                    ],
                    [
                        '$ref' => '#/definitions/lolevents'
                    ],
                    [
                        '$ref' => '#/definitions/dota2events'
                    ]
                ],
            ];
        }

        if($key=='/battles/detail'){
            $val['get']['responses'][200]['schema']['properties']['battle_detail']=[
                'type' => 'object',
                'nullable' => true,
                'description' => '对局详情',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/battle_detail_csgo'
                    ],
                    [
                        '$ref' => '#/definitions/battle_detail_lol'
                    ],
                    [
                        '$ref' => '#/definitions/battle_detail_dota2'
                    ],
                ],
            ];
        }

        if($key=='/battles/list'){
            $val['get']['responses'][200]['schema']['items']['properties']['battle_detail']=[
                'type' => 'object',
                'nullable' => true,
                'description' => '对局详情',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/battle_detail_csgo'
                    ],
                    [
                        '$ref' => '#/definitions/battle_detail_lol'
                    ],
                    [
                        '$ref' => '#/definitions/battle_detail_dota2'
                    ],
                ],
            ];
        }

        if($key=='/tournaments/groupdetail'){
            $val['get']['responses'][200]['schema']=[
                'type' => 'object',
                'oneOf' => [
                    [
                        '$ref' => '#/definitions/group_league'
                    ],
                    [
                        '$ref' => '#/definitions/group_group'
                    ],
                    [
                        '$ref' => '#/definitions/group_bracket'
                    ],
                ],
            ];
        }

        if($key=='/tournaments/branches'){
            $val['get']['responses'][200]['schema']['properties']['branches']=[
                'type' => 'array',
                'description' => '子项目',
                'items' => [
                    'type' => 'object',
                    'nullable' => true,
                    'description' => '子项目',
                    'oneOf' => [
                        [
                            '$ref' => '#/definitions/branch_catalog'
                        ],
                        [
                            '$ref' => '#/definitions/branch_subtournament'
                        ],
                        [
                            '$ref' => '#/definitions/branch_stage'
                        ],
                        [
                            '$ref' => '#/definitions/branch_group_league'
                        ],
                        [
                            '$ref' => '#/definitions/branch_group_group'
                        ],
                        [
                            '$ref' => '#/definitions/branch_group_bracket'
                        ],
                    ],
                ],
            ];
        }

        if($key=='/tournaments/branches' || $key=='/tournaments/detail'){
            $val['get']['responses'][200]['schema']['properties']['teams']['items']['properties']['team_snapshot']['properties']['players']['items']['properties']
            ['player_snapshot']['properties']['role'] = $role;
        }
        if($key=='/tournaments/list' || $key=='/tournaments/upcoming' || $key=='/tournaments/ongoing' || $key=='/tournaments/past'){
            $val['get']['responses'][200]['schema']['items']['properties']['teams']['items']['properties']['team_snapshot']
            ['properties']['players']['items']['properties']['player_snapshot']['properties']['role'] = $role;
        }
        if($key == '/teams/list'){
            $val['get']['responses'][200]['schema']['items']['properties']['history_players']['items']['properties']['role'] = $role;
            $val['get']['responses'][200]['schema']['items']['properties']['players']['items']['properties']['role'] = $role;
        }
        if($key == '/teams/detail'){
            $val['get']['responses'][200]['schema']['properties']['history_players']['items']['properties']['role'] = $role;
            $val['get']['responses'][200]['schema']['properties']['players']['items']['properties']['role'] = $role;
        }
        if($key == '/players/list'){
            $val['get']['responses'][200]['schema']['items']['properties']['role'] = $role;
        }
        if($key == '/players/detail'){
            $val['get']['responses'][200]['schema']['properties']['role'] = $role;
        }

    }
    $dataJson = json_encode($data);
    file_put_contents('swagger_Other.json',$dataJson);
?>
